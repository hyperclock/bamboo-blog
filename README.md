![alt Bamboo Blog](public/images/logo/logo_v1_240-80.png)

Bamboo Blog
===========

is built using the [Symfony Framework](https://symfony.com), the latest LTS version ist what we use. Which is the 4.4 version as we started the project.

## Planned Features
* Full i18n capabilities
  * English (default)
  * German (secondary)
  * easily add more languages
* Blog
* Easy configuration
* Admin Panel
* User Administration
* Cookie Popup Notice

## Documentation
keep an eye on the [wiki](../../wikis).

## License
***BSD 3-Clause "New" or "Revised" License***

A permissive license similar to the BSD 2-Clause License, but with a 3rd clause that prohibits others from using the name of the project or its contributors to promote derived products without written consent.

See [LICENSE](LICENSE) for details.

**Permissions**
* Commercial use
* Modification
* Distribution
* Private use

**Limitations**
* No Liability
* No Warranty

**Conditions**
* License and copyright notice
  * -must be included with the software.

## Contibuting
See [CONTRIBUTING](CONTRIBUTING.md) for details.


## Change Log
See [CHANGELOG](CHANGELOG) for details.
